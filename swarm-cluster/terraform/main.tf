terraform {
  required_providers {
    nutanix = {
      source = "nutanix/nutanix"
      version = "1.2.0"
    }
  }
}

provider "nutanix" {
  username = var.prism_username
  password = var.prism_password
  endpoint = var.cluster_endpoint
  insecure = true
  wait_timeout = 10
  port = 9440
}

#resource "nutanix_subnet" "iaac" {
#  name = var.subnet_name
#  cluster_uuid = data.nutanix_clusters.clusters.entities.0.metadata.uuid
#
#  vlan_id = var.vlan_id
#  subnet_type = "VLAN"
#  prefix_length = var.prefix_length 
#  default_gateway_ip = var.gw_ip
#  subnet_ip = var.subnet_ip
#  ip_config_pool_list_ranges = var.ip_config_pool_list
#  dhcp_domain_name_server_list = var.dhcp_dns_list
#}

resource "nutanix_image" "ubuntu_image" {
	name = "Ubuntu"
	description = "Ubuntu"
	source_uri = "https://cloud-images.ubuntu.com/focal/current/focal-server-cloudimg-amd64.img"
}

resource "nutanix_virtual_machine" "swarm_host_" {
  count = var.number_of_vms
  name                 = "ubuntu_swarm_host_${count.index + 1}"
  cluster_uuid         = data.nutanix_clusters.clusters.entities.0.metadata.uuid
  num_vcpus_per_socket = var.number_of_vcpus_sql
  num_sockets          = var.number_of_sockets_sql
  memory_size_mib      = var.memory_size_sql
  guest_customization_cloud_init_user_data = var.startup_script 
  
	nic_list {
    subnet_uuid = var.subnet_uuid 
  }
  
  disk_list {
		disk_size_mib = 100000
    disk_size_bytes = 104857600000 
		
    data_source_reference = {
			kind = "image"
			uuid = nutanix_image.ubuntu_image.id
		}  

		device_properties {
			disk_address = {
				device_index = 0
				adapter_type = "SCSI"
			}
		
			device_type = "DISK"
		}
	}
}

data "nutanix_clusters" "clusters" { 
}

