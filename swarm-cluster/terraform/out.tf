output "swarm_hosts_ip_address" {
  value = nutanix_virtual_machine.swarm_host_[*].nic_list_status.0.ip_endpoint_list[0]["ip"]
}
output "cluster" {
  value = data.nutanix_clusters.clusters.entities.0.metadata.uuid
}
