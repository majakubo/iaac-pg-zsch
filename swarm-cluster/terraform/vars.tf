#Tools helpful for obtaining infrastructure info are listed in Terraform/README.md                                                                                                                                 
#Cluster properties
variable "cluster_endpoint" { }
variable "prism_username" {  }
variable "prism_password" { sensitive = true  }

#SQL vm properties
variable "number_of_vms" {   }
variable "number_of_vcpus_sql" {  }
variable "number_of_sockets_sql" {  }
variable "memory_size_sql" {  }
# list of disks images UUID that are used to create each VM
variable "disks_uuids_list" { 
  type = list
}

#Subnet data
variable "vlan_id" {}
variable "prefix_length" {}
variable "gw_ip" {}
variable "subnet_name" {}
variable "subnet_ip" {}
variable "ip_config_pool_list" {}
variable "dhcp_dns_list" {}
variable "subnet_uuid" {}
variable "startup_script" {
default = "I2Nsb3VkLWNvbmZpZwpkaXNhYmxlX3Jvb3Q6IGZhbHNlCmFwdF91cGdyYWRlOiB0cnVlCnBhY2thZ2VzOgotIGRvY2tlci5pbwp1c2VyczoKLSBuYW1lOiByb290CiAgc3NoLWF1dGhvcml6ZWQta2V5czoKICAgIC0gc3NoLXJzYSBBQUFBQjNOemFDMXljMkVBQUFBREFRQUJBQUFCZ1FEQmNtT3pEOUNEcXVLeFRHQjFuWXhVZnJmTHNINE1sVjV6QVNuVDRmeCtvTzM0NFNPOVE5UlVkcXpEOE9PRzAzN0NHeDZaRXJwSEpxbnR0cUhsc2FONWNOYmsvekgrOFVRZlliUUdOb09TY0pKT0p3ejNJY2FCRFJsMUtRbzFhWW95TUIvMzcxcFcrVEZpYW0rbVRSUUZxTk4rVndlWnlBaXRuK0VTSGlvaTdzOG52ZmlROUc5SGRuckNYU2FFaXpCSW1Ld2tuRlIvdTlUU2x0cHpGcmFvdlVGcTdKQnZ4ZjFneVlrOWIxeUw1NklmZE1SL0RldkdFSTdvMUFQMjQ5R0YwNU1XVDdEaHZxZ0hWYVVwOXZFZWd0WW1jS3JDbkRxVkZqQTB5YVQ4MlBjVTRTS0xqaE1meGQxWm51R3RGRnZxNEJnQmRHY2p4WUpqaGZFZjFjeDd4cFQyRDczWnl1OVBsY1doOFFoUlFMbkRtdWV3QXVTR2pvYVVsWm1sMUx2OFFoT0VTTDlLUExCekNmdU41RitZRFBOVmhCSGhITTEwaENObUthdVJ5dFhqdEJyWjljU24zbXdqV0d2OFEzUlEvNUlxdkRyQTFDUDl5SWRubzB6NW5vMk9ZN3ExdkJBOFIwdWtFRW9CZ0cyVVpiRG0yR21xZjYrMzV4bWxVemM9IG1hamFrdUBtYWNpZWpqYS1NT0JMCi0gbmFtZTogbWoKICBzc2gtYXV0aG9yaXplZC1rZXlzOgogICAgLSBzc2gtcnNhIEFBQUFCM056YUMxeWMyRUFBQUFEQVFBQkFBQUJnUURCY21PekQ5Q0RxdUt4VEdCMW5ZeFVmcmZMc0g0TWxWNXpBU25UNGZ4K29PMzQ0U085UTlSVWRxekQ4T09HMDM3Q0d4NlpFcnBISnFudHRxSGxzYU41Y05iay96SCs4VVFmWWJRR05vT1NjSkpPSnd6M0ljYUJEUmwxS1FvMWFZb3lNQi8zNzFwVytURmlhbSttVFJRRnFOTitWd2VaeUFpdG4rRVNIaW9pN3M4bnZmaVE5RzlIZG5yQ1hTYUVpekJJbUt3a25GUi91OVRTbHRwekZyYW92VUZxN0pCdnhmMWd5WWs5YjF5TDU2SWZkTVIvRGV2R0VJN28xQVAyNDlHRjA1TVdUN0RodnFnSFZhVXA5dkVlZ3RZbWNLckNuRHFWRmpBMHlhVDgyUGNVNFNLTGpoTWZ4ZDFabnVHdEZGdnE0QmdCZEdjanhZSmpoZkVmMWN4N3hwVDJENzNaeXU5UGxjV2g4UWhSUUxuRG11ZXdBdVNHam9hVWxabWwxTHY4UWhPRVNMOUtQTEJ6Q2Z1TjVGK1lEUE5WaEJIaEhNMTBoQ05tS2F1Unl0WGp0QnJaOWNTbjNtd2pXR3Y4UTNSUS81SXF2RHJBMUNQOXlJZG5vMHo1bm8yT1k3cTF2QkE4UjB1a0VFb0JnRzJVWmJEbTJHbXFmNiszNXhtbFV6Yz0gbWFqYWt1QG1hY2llamphLU1PQkwKICBwYXNzd29yZDogaW50ZWwxMjMKICBjaHBhc3N3ZDogeyBleHBpcmU6IEZhbHNlIH0KICBzc2hfcHdhdXRoOiBUcnVlCiAgc3VkbzogWydBTEw9KEFMTCkgTk9QQVNTV0Q6QUxMJ10KICBzaGVsbDogL2Jpbi9iYXNo"
}
